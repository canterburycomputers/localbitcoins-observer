README: Localbitcoins observer
======

This command line PHP app will download trade data for specified currencies from localbitcoins public API.

Features:

*     Update existing csv file without redownloading all the data.
*     Specify an array of currencies to do all updates with one command.

How do I do the thing????
-------------------

    php iwantdata.php

To change the currencies just modify the first line of code which is currently:

    $currencies = array('CHF','EUR','GBP');


Troubleshooting
-------------------

If you see the 503 error the script will break (stop at that point).  This is usually when the API is overloaded.  Just run the script again and it should resume.