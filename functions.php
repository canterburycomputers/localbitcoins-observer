<?php

function array_to_csv($array,$fields, $header_row = true, $col_sep = ",", $row_sep = "\n", $qut = '"')
{
        if (!is_array($array)) return false;
        $output = '';
        //Header row.
        if ($header_row)
        {

                foreach ($array[0] as $key => $val)
                {
                    if(in_array($key,$fields)){
                        //Escaping quotes.
                        $key = str_replace($qut, "$qut$qut", $key);
                        $output .= "$col_sep$qut$key$qut";
                    }
                }
                $output = substr($output, 1)."\n";
        }
        //Data rows.
        foreach ($array as $key => $val)
        {
                $tmp = '';
                foreach ($val as $cell_key => $cell_val)
                {
                    if(in_array($cell_key,$fields)){
                        //Escaping quotes.
                        if($cell_key == "date"){
            $cell_val = date('d-m-Y h:i:s', $cell_val);  // A (")   for the timezone
            }
                        $cell_val = str_replace($qut, "$qut$qut", $cell_val);
                        $tmp .= "$col_sep$qut$cell_val$qut";
                    }
                }
                $output .= substr($tmp, 1).$row_sep;
        }

        return $output;
}

function lastline($file){
    //function to return last line of text file
    $line = '';

    $f = fopen($file, 'r');
    $cursor = -1;

    fseek($f, $cursor, SEEK_END);
    $char = fgetc($f);

    /**
     * Trim trailing newline chars of the file
     */
    while ($char === "\n" || $char === "\r") {
        fseek($f, $cursor--, SEEK_END);
        $char = fgetc($f);
    }

    /**
     * Read until the start of file or first newline char
     */
    while ($char !== false && $char !== "\n" && $char !== "\r") {
        /**
         * Prepend the new char
         */
        $line = $char . $line;
        fseek($f, $cursor--, SEEK_END);
        $char = fgetc($f);
    }

    return $line;
}

function dothething($fh,$currency,$since = ""){
    echo "Requesting: https://localbitcoins.com/bitcoincharts/".$currency."/trades.json".$since."\n";
    $str = file_get_contents("https://localbitcoins.com/bitcoincharts/".$currency."/trades.json".$since);
    $arr = json_decode($str, true);
    $fields = array("tid", "date", "amount", "price");
    if($since==""){
        //only show the header row if we are starting from the beginning.
        $saveData = array_to_csv($arr, $fields, true);
    }else{
        $saveData = array_to_csv($arr, $fields, false);
    }
    fwrite($fh, $saveData);
    $ret = end($arr);
    $ret['count'] = count($arr);
  return $ret;
}

?>