<?php
$currencies = array('CHF','EUR','GBP');
$currency = "CHF";
//Set the timezone to UTC
date_default_timezone_set('UTC'); 

$filewriteerror = "Unable to write csv file.  Please make sure the php user has permission to write files or run this script with sudo.  eg: \n sudo php iwantdata.php \n\n I that still doesn't work make sure you're not on a read only filesystem or the file is not already open";

$bitcoin = '                 ,.=ctE55ttt553tzs.,                               
             ,,c5;z==!!::::  .::7:==it3>.,                         
          ,xC;z!::::::    ::::::::::::!=c33x,                      
        ,czz!:::::  ::;;..===:..:::   ::::!ct3.                    
      ,C;/.:: :  ;=c!:::::::::::::::..      !tt3.                  
     /z/.:   :;z!:::::J  :E3.  E:::::::..     !ct3.                
   ,E;F   ::;t::::::::J  :E3.  E::.     ::.     \ttL               
  ;E7.    :c::::F******   **.  *==c;..    ::     Jttk              
 .EJ.    ;::::::L                   "\:.   ::.    Jttl             
 [:.    :::::::::773.    JE773zs.     I:. ::::.    It3L            
;:[     L:::::::::::L    |t::!::J     |::::::::    :Et3            
[:L    !::::::::::::L    |t::;z2F    .Et:::.:::.  ::[13            
E:.    !::::::::::::L               =Et::::::::!  ::|13            
E:.    (::::::::::::L    .......       \:::::::!  ::|i3            
[:L    !::::      ::L    |3t::::!3.     ]::::::.  ::[13            
!:(     .:::::    ::L    |t::::::3L     |:::::; ::::EE3            
 E3.    :::::::::;z5.    Jz;;;z=F.     :E:::::.::::II3[            
 Jt1.    :::::::[                    ;z5::::;.::::;3t3             
  \z1.::::::::::l......   ..   ;.=ct5::::::/.::::;Et3L             
   \t3.:::::::::::::::J  :E3.  Et::::::::;!:::::;5E3L              
    "cz\.:::::::::::::J   E3.  E:::::::z!     ;Zz37`               
      \z3.       ::;:::::::::::::::;="      ./355F                 
        \z3x.         ::~======="         ,c253F                   
          "tz3=.                      ..c5t32^                     
             "=zz3==...         ...=t3z13P^                        
                 `*=zjzczIIII3zzztE3>*^`                           ';

require_once("functions.php");


echo "localbitcoins-observer started \n";
echo $bitcoin."\n";
$start = microtime(true);
$totalcount = 0;
foreach($currencies as $currency){
    $logFile = "LBC".$currency.".csv";
    $fh = fopen($logFile, 'a') or die("can't open/create file");
    if(!is_writable($logFile)){
        die($filewriteerror);
    }
    //get the last id from file (so we don't download all again)
    $ll = lastline($logFile);
    $tid = explode(",", $ll)[0];
    //remove the quotes
    $tid = str_replace('"', "", $tid);
    $done = false;
    if(is_numeric($tid)){
        echo "Resuming data fetching from id: $tid \n\n";
        $since = "?since=".$tid;
    }else{
        echo "Existing data not found, downloading all to present\n\n";
        $since = "";    
    }

    $count = 0;
    while($done == false){
        if($ret=dothething($fh,$currency,$since)){
            $count += $ret["count"];
            echo "Added ".$ret["count"]." records for ".$currency."\n";
            if($ret["count"]==0){
                $done = true;
            }else{
                $since="?since=".$ret["tid"];
            }
        }else{
            $done = true;
        }
    }
    echo "\n\nCOMPLETED ".$currency.". ".$count." records updated.\n\n" ;
    $totalcount += $count;
}
$time_elapsed = microtime(true) - $start;
echo "ALL COMPLETED.  Total updated records: ".$totalcount.".  Time taken: ".$time_elapsed." seconds.  Have a nice day.";

?>
